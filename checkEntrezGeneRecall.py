# Before running this script
# run the following lines from the command line
# to obtain the data

# wget ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/GENE_INFO/Mammalia/Homo_sapiens.gene_info.gz
# gzcat Homo_sapiens.gene_info.gz |awk -F'\t' '{print $2 "\t" $12}'>>entrezGenes.tsv


import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
from pywikibot.data import api
import pprint

# filehandling
entrezgeneOutput = open("entrezGeneInWikiData.wiki", "w")
f = open('entrezGenes.tsv', 'r')

# Connect to wikidata
site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

params = { 'action' :'wbsearchentities' ,
           'format' : 'json' ,
           'language' : 'en',
           'type' : 'item',
           'search': '',
         }
i = 0;
for line in f:
     outputLine = "";
     entrezgeneline = line.split('\t')
     entrezgeneId= entrezgeneline[0]
     entrezgeneName = entrezgeneline[1].rstrip()
     i+=1
     print str(i) + ": "+ entrezgeneId,
     outputLine = outputLine + entrezgeneId+"\t"  
     params['search']= entrezgeneName
     request = api.Request(site=site,**params)
     data = request.submit()
     pp = pprint.PrettyPrinter(indent=4)
     # pp.pprint(data['search'])
     for page in data['search']:
      #  print len(page),
      #  pp.pprint(page)
        print '[['+page["id"]+']]',
        outputLine = outputLine + '[['+page["id"] +']]' + "\t"
     print 
     entrezgeneOutput.write(outputLine+"\n")

f.close
entrezgeneOutput.close
