import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
import pprint

def addStatement(defrepo, defitem, propertyKey, propertyValue, importedfrom):
    claim = pywikibot.Claim(defrepo, propertyKey)
    claim.setTarget(propertyValue)
    statedin = pywikibot.Claim(defrepo, 'p143') # Imported from
    statedin.setTarget(importedfrom)
    defitem.addClaim(claim)
    claim.addSource(statedin, bot=True)

# Login to wikidata
site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

#references 
reference2ensembl = pywikibot.ItemPage(repo, 'Q1344256')
humanSpeciesPage = pywikibot.ItemPage(repo, u'P5')

# Select an existing page FIXME need to implement a function to create a page if an entry doesn't exist
item = pywikibot.ItemPage(repo, "Q17501046")
dictionary = item.get()

ensemblId = "ENSG00000121207"
f = open('ensemblGenes.tsv', 'r')
ensemblGeneLabel = dict() # Dictionary to capture Ensembl identifiers and their label
for line in f:
    ensemblline = line.split('\t')
    ensemblGeneLabel[ensemblline[0]] = ensemblline[1] # ensemblline[0] = ensemblId, ensemblline[1] = primary label
print len(ensemblGeneLabel)
print ensemblGeneLabel[ensemblId]

# Get  details from MygeneInfo
req=urllib2.Request("http://mygene.info/v2/gene/"+ensemblId, None, {'user-agent':'ProteinBoxBot'})
print "http://mygene.info/v2/gene/"+ensemblId
opener = urllib2.build_opener()
f = opener.open(req)
mygeneinfo = simplejson.load(f)

#Add Homologene
homologeneid = mygeneinfo[1]["homologene"]["id"]
print "Adding homologeneid: "+str(homologeneid)
addStatement(repo, item, 'P593', str(homologeneid), reference2ensembl) # P593 = homologene id


