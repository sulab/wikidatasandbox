import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
import pprint

def addStatement(defrepo, defitem, propertyKey, propertyValue, importedfrom):
    claim = pywikibot.Claim(defrepo, propertyKey)
    claim.setTarget(propertyValue)
    statedin = pywikibot.Claim(defrepo, 'p143') # Imported from
    statedin.setTarget(importedfrom)
    defitem.addClaim(claim)
    claim.addSource(statedin, bot=True)

# Login to wikidata
site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

#references 
reference2ensembl = pywikibot.ItemPage(repo, 'Q1344256')
humanSpeciesPage = pywikibot.ItemPage(repo, u'P5')

# Select an existing page FIXME need to implement a function to create a page if an entry doesn't exist
item = pywikibot.ItemPage(repo, "Q17501046")
dictionary = item.get()

ensemblId = "ENSG00000121207"
f = open('ensemblGenes.tsv', 'r')
ensemblGeneLabel = dict() # Dictionary to capture Ensembl identifiers and their label
for line in f:
    ensemblline = line.split('\t')
    ensemblGeneLabel[ensemblline[0]] = ensemblline[1] # ensemblline[0] = ensemblId, ensemblline[1] = primary label
print len(ensemblGeneLabel)
print ensemblGeneLabel[ensemblId]

# Get  details from MygeneInfo
req=urllib2.Request("http://mygene.info/v2/gene/"+ensemblId, None, {'user-agent':'ProteinBoxBot'})
print "http://mygene.info/v2/gene/"+ensemblId
opener = urllib2.build_opener()
f = opener.open(req)
mygeneinfo = simplejson.load(f)

#Add chromosome
chromosome = mygeneinfo[0]["genomic_pos"]["chr"]
chromosomes = dict()
chromosomes['1'] = "Q430258"
chromosomes['2'] = "Q638893"
chromosomes['3'] = "Q668633"
chromosomes['4'] = "Q836605"
chromosomes['5'] = "Q840741"
chromosomes['6'] = "Q540857"
chromosomes['7'] = "Q657319"
chromosomes['8'] = "Q572848"
chromosomes['9'] = "Q840604"
chromosomes['10'] = "Q840737"
chromosomes['11'] = "Q847096"
chromosomes['12'] = "Q847102"
chromosomes['13'] = "Q840734"
chromosomes['14'] = "Q138955"
chromosomes['15'] = "Q765245"
chromosomes['16'] = "Q742870"
chromosomes['17'] = "Q220677"
chromosomes['18'] = "Q780468"
chromosomes['19'] = "Q510786"
chromosomes['20'] = "Q666752"
chromosomes['21'] = "Q753218"
chromosomes['22'] = "Q753805"
chromosomes['22'] = "Q753805"
chromosomes['X'] = "Q61333" 
chromosomes['Y'] = "Q202771"


print "Adding Chromosome: "+str(chromosome)

chromosomepage = pywikibot.ItemPage(repo, chromosomes[str(chromosome)])
addStatement(repo, item, 'P1057', chromosomepage, reference2ensembl) # P1057 = chromosome 


