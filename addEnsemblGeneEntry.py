import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
import pprint

def addStatement(defrepo, defitem, propertyKey, propertyValue, importedfrom):
    claim = pywikibot.Claim(defrepo, propertyKey)
    claim.setTarget(propertyValue)
    statedin = pywikibot.Claim(defrepo, 'p143') # Imported from
    statedin.setTarget(importedfrom)
    defitem.addClaim(claim)
    claim.addSource(statedin, bot=True)

# Login to wikidata
site = pywikibot.Site("wikidata", "wikidata")
# site = pywikibot.Site("wikidata", "test")
repo = site.data_repository()

#references 
reference2ensembl = pywikibot.ItemPage(repo, 'Q1344256')  
humanSpeciesPage = pywikibot.ItemPage(repo, u'P5')
genePage = pywikibot.ItemPage(repo, u'Q7187')
 
# Select an existing page FIXME need to implement a function to create a page if an entry doesn't exist
item = pywikibot.ItemPage(repo, "Q17501046")
dictionary = item.get()

ensemblId = "ENSG00000121207"
f = open('ensemblGenes.tsv', 'r')
ensemblGeneLabel = dict() # Dictionary to capture Ensembl identifiers and their label
for line in f:
    ensemblline = line.split('\t')
    ensemblGeneLabel[ensemblline[0]] = ensemblline[1] # ensemblline[0] = ensemblId, ensemblline[1] = primary label
print len(ensemblGeneLabel)
print ensemblGeneLabel[ensemblId]

# Add the Ensembl gene ID and a reference to Ensembl
addStatement(repo, item, 'P594', ensemblline[0], reference2ensembl) # P594 = Ensembl Gene Id

# Add species
addStatement(repo, item, 'P703', humanSpeciesPage, reference2ensembl) #P703 = found in taxon

# Add type "subclass of" P279 gene Q7187
addStatement(repo, item, 'P279', genePage, reference2ensembl) #P279 = subclass of

# Get  details from MygeneInfo
req=urllib2.Request("http://mygene.info/v2/gene/"+ensemblId, None, {'user-agent':'Micelio'})
print "http://mygene.info/v2/gene/"+ensemblId
opener = urllib2.build_opener()
f = opener.open(req)
mygeneinfo = simplejson.load(f)

# Add Ensembl transcript Id's
ensemblEntries =  mygeneinfo[0]["ensembl"]
#ensemblTranscriptclaim = pywikibot.Claim(repo, u'P704') #Transcript ID
for ensemblEntry in ensemblEntries["transcript"]:
    print "adding Transcript id: "+ ensemblEntry
    addStatement(repo, item, 'P704', ensemblEntry, reference2ensembl) #  P704 = Ensembl transcript ID
    # ensemblTranscriptclaim.setTarget(ensemblEntry)
    # item.addClaim(ensemblTranscriptclaim)
    # ensemblTranscriptclaim.addSource(statedin, bot=True)


#Add entrez gene
entrezgene = mygeneinfo[0]["entrezgene"] #FIXME Allow multiple entrez genes in an array
print "Adding EntrezGene ID: "+str(entrezgene)
addStatement(repo, item, 'P351', str(entrezgene), reference2ensembl) # P351 = entrezgene id

# Add Gene Symbol
genesymbol = mygeneinfo[0]["symbol"]
print "Adding gene Symbol" 
addStatement(repo, item, 'P353', genesymbol, reference2ensembl)

# Add HGNC ID
hgncid = mygeneinfo[1]["HGNC"]
print "Adding HGNC ID"
addStatement(repo, item, 'P354', str(hgncid), reference2ensembl) # P354 = HGNC ID

#Add Homologene
homologeneid = mygeneinfo[1]["homologene"]["id"]
print "Adding homologeneid: "+str(homologeneid)
addStatement(repo, item, 'P593', str(homologeneid), reference2ensembl) # P593 = homologene id


#Add refseq RNA entries
for i in range(len(mygeneinfo)): #FIXME Why does mygeneinfo return two items both with refseq identifiers
  refseqEntries =  mygeneinfo[i]["refseq"]
  for refseqRnaEntry in refseqEntries["rna"]:
    print "adding refseq rna id: "+ refseqRnaEntry
    addStatement(repo, item, 'P639', refseqRnaEntry, reference2ensembl) #  P639 = RefSeq RNA ID

#Add chromosome
chromosome = mygeneinfo[0]["genomic_pos"]["chr"]
chromosomes = dict()
chromosomes['1'] = "Q430258"
chromosomes['2'] = "Q638893"
chromosomes['3'] = "Q668633"
chromosomes['4'] = "Q836605"
chromosomes['5'] = "Q840741"
chromosomes['6'] = "Q540857"
chromosomes['7'] = "Q657319"
chromosomes['8'] = "Q572848"
chromosomes['9'] = "Q840604"
chromosomes['10'] = "Q840737"
chromosomes['11'] = "Q847096"
chromosomes['12'] = "Q847102"
chromosomes['13'] = "Q840734"
chromosomes['14'] = "Q138955"
chromosomes['15'] = "Q765245"
chromosomes['16'] = "Q742870"
chromosomes['17'] = "Q220677"
chromosomes['18'] = "Q780468"
chromosomes['19'] = "Q510786"
chromosomes['20'] = "Q666752"
chromosomes['21'] = "Q753218"
chromosomes['22'] = "Q753805"
chromosomes['22'] = "Q753805"
chromosomes['X'] = "Q61333"
chromosomes['Y'] = "Q202771"


print "Adding Chromosome: "+str(chromosome)

chromosomepage = pywikibot.ItemPage(repo, chromosomes[str(chromosome)])
addStatement(repo, item, 'P1057', chromosomepage, reference2ensembl) # P1057 = chromosome FIXME: Link to Wikidata page
