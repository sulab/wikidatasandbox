# Import the required libraries
import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
from pywikibot.data import api
import pprint

# Login to wikidata
site = pywikibot.Site("wikidata", "wikidata")
# site = pywikibot.Site("wikidata", "test")
repo = site.data_repository()

def getClaims(wdItem, claimProperty):
    params = {
    			'action' :'wbgetclaims' ,
                'entity' : wdItem,
    			'property': claimProperty,
             }
    request = api.Request(site=site,**params)
    return request.submit()
   
def countClaims(wdItem, claimProperty):
    data = getClaims(wdItem, claimProperty)
    return len(data["claims"][wdProperty])

def claimExists(wdItem, claimProperty, claimValue):
    claimexists = False
    data = getClaims(wdItem, claimProperty)
    for claim in data["claims"][wdProperty]:
        if claim['mainsnak']['datavalue']['value'] == claimValue:
            claimexists = True
            break
    return claimexists

item = 'Q17543707'
wdProperty = "P594"
pp = pprint.PrettyPrinter(indent=4)

ensemblGeneIDs = getClaims(item, wdProperty)

pp.pprint(ensemblGeneIDs)

pp.pprint(countClaims(item, wdProperty))

print claimExists(item, wdProperty, 'ENSG000001758999')
'''
if isinstance(ensemblGeneIDs["claims"][wdProperty], list):
    for ensemblGene in ensemblGeneIDs["claims"][wdProperty]:
        print ensemblGene['mainsnak']['datavalue']['value']
# print ensemblGeneIDs["claims"][wdProperty].keys()

{   u'claims': {   u'P594': [   {   u'id': u'Q17543707$F1818BBF-C3DA-40EC-985E-B6DDE267B561',
                                    u'mainsnak': {   u'datatype': u'string',
                                                     u'datavalue': {   u'type': u'string',
                                                                       u'value': u'ENSG00000175899'},
                                                     u'property': u'P594',
                                                     u'snaktype': u'value'},
                                    u'rank': u'normal',
'''