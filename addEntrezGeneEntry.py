import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
import pprint

def addStatement(defrepo, defitem, propertyKey, propertyValue, importedfrom):
    claim = pywikibot.Claim(defrepo, propertyKey)
    claim.setTarget(propertyValue)
    statedin = pywikibot.Claim(defrepo, 'p143') # Imported from
    statedin.setTarget(importedfrom)
    defitem.addClaim(claim)
    claim.addSource(statedin, bot=True)

def addIdentifier(subrepo, subitem, identifierVar, wikidataProperty, reference):
    if isinstance(identifierVar, basestring):
        addStatement(subrepo, subitem, wikidataProperty, identifierVar, reference)    
    else:
        for identifier in identifierVar:
            addStatement(subrepo, subitem, wikidataProperty, identifier, reference)

#Chromosomes
chromosomes = dict()
chromosomes['1'] = "Q430258"
chromosomes['2'] = "Q638893"
chromosomes['3'] = "Q668633"
chromosomes['4'] = "Q836605"
chromosomes['5'] = "Q840741"
chromosomes['6'] = "Q540857"
chromosomes['7'] = "Q657319"
chromosomes['8'] = "Q572848"
chromosomes['9'] = "Q840604"
chromosomes['10'] = "Q840737"
chromosomes['11'] = "Q847096"
chromosomes['12'] = "Q847102"
chromosomes['13'] = "Q840734"
chromosomes['14'] = "Q138955"
chromosomes['15'] = "Q765245"
chromosomes['16'] = "Q742870"
chromosomes['17'] = "Q220677"
chromosomes['18'] = "Q780468"
chromosomes['19'] = "Q510786"
chromosomes['20'] = "Q666752"
chromosomes['21'] = "Q753218"
chromosomes['22'] = "Q753805"
chromosomes['22'] = "Q753805"
chromosomes['X'] = "Q61333"
chromosomes['Y'] = "Q202771"

# Login to wikidata
site = pywikibot.Site("wikidata", "wikidata")
# site = pywikibot.Site("wikidata", "test")
repo = site.data_repository()

#references 
reference2entrezGene = pywikibot.ItemPage(repo, 'Q1345229')  
humanSpeciesPage = pywikibot.ItemPage(repo, u'P5')
genePage = pywikibot.ItemPage(repo, u'Q7187')
 
# Select an existing page FIXME need to implement a function to create a page if an entry doesn't exist
item = pywikibot.ItemPage(repo, "Q17552950")
dictionary = item.get()

entrezGeneId = "22914"
f = open('entrezGenes.tsv', 'r')
entrezGeneLabel = dict() # Dictionary to capture Entrez identifiers and their label
for line in f:
    entrezGeneline = line.split('\t')
    entrezGeneLabel[entrezGeneline[0]] = entrezGeneline[1] # entrezGeneline[0] = entrezGeneId, entrezGeneline[1] = primary label
print len(entrezGeneLabel)
print entrezGeneLabel[entrezGeneId]

# Add species
addStatement(repo, item, 'P703', humanSpeciesPage, reference2entrezGene) #P703 = found in taxon

# Add type "subclass of" P279 gene Q7187
addStatement(repo, item, 'P279', genePage, reference2entrezGene) #P279 = subclass of

# Get  details from MygeneInfo
req=urllib2.Request("http://mygene.info/v2/gene/"+entrezGeneId, None, {'user-agent':'ProteinBoxBot'})
print "http://mygene.info/v2/gene/"+entrezGeneId
opener = urllib2.build_opener()
f = opener.open(req)
mygeneinfo = simplejson.load(f)


# Add Ensembl Id's
if "ensembl" in mygeneinfo: 
    ensemblEntries =  mygeneinfo["ensembl"]  # P594 = Entrez Gene Id
    if isinstance(ensemblEntries, list):
       mygeneinfoMultiples.write(entrezGeneId+": "+"http://mygene.info/v2/gene/"+entrezGeneId+": Multiple Ensembl entries in mygene.info")
    else:
       # Ensembl Gene
       addIdentifier(repo, item, ensemblEntries["gene"], 'P594', reference2entrezGene) # P594 = Ensembl Gene Id
       # Ensembl Transcript
       addIdentifier(repo, item, ensemblEntries["transcript"], 'P704', reference2entrezGene) # P704 = Ensembl Transcript Id

if "symbol" in mygeneinfo: addIdentifier(repo, item, mygeneinfo["symbol"], 'P353', reference2entrezGene) # P353 = gene symbol
if "HGNC" in mygeneinfo: addIdentifier(repo, item, mygeneinfo["HGNC"], 'P354', reference2entrezGene) # P354 = HGNC ID
if "homologene" in mygeneinfo: addIdentifier(repo, item, str(mygeneinfo["homologene"]["id"]), 'P593', reference2entrezGene) # P593 = homologene id
    
if "refseq" in mygeneinfo:
     refseqEntries =  mygeneinfo["refseq"]
     if "rna" in refseqEntries: addIdentifier(repo, item, refseqEntries["rna"], 'P639', reference2entrezGene) # P639 = RefSeq RNA ID    
#Add chromosome
if "genomic_pos" in mygeneinfo: 
    chromosome = mygeneinfo["genomic_pos"]["chr"]
    chromosomepage = pywikibot.ItemPage(repo, chromosomes[str(chromosome)])
    addStatement(repo, item, 'P1057', chromosomepage, reference2entrezGene)

'''
# Add Ensembl Id's
ensemblEntries =  mygeneinfo["ensembl"]  # P594 = Entrez Gene Id

# Ensembl Gene
addIdentifier(repo, item, ensemblEntries["gene"], 'P594', reference2entrezGene) # P594 = Ensembl Gene Id

# Ensembl Transcript
addIdentifier(repo, item, ensemblEntries["transcript"], 'P704', reference2entrezGene) # P704 = Ensembl Transcript Id




#Add entrez gene
entrezgene = mygeneinfo["entrezgene"] 

print "Adding EntrezGene ID: "+str(entrezgene)
addIdentifier(repo, item, ensemblEntries["gene"], 'P351', reference2entrezGene) # P351 = Entrez gene

# Add Gene Symbol
print "Adding gene Symbol" 
addIdentifier(repo, item, mygeneinfo["symbol"], 'P353', reference2entrezGene) 

# Add HGNC ID
hgncid = mygeneinfo["HGNC"]
print "Adding HGNC ID"
addIdentifier(repo, item, mygeneinfo["HGNC"], 'P354', reference2entrezGene) # P354 = HGNC ID

#Add Homologene
print "Adding homologeneid: "+str(mygeneinfo["homologene"]["id"])
addIdentifier(repo, item, str(mygeneinfo["homologene"]["id"]), 'P593', reference2entrezGene) # P593 = homologene id


#Add refseq RNA entries
# for i in range(len(mygeneinfo)): #FIXME Why does mygeneinfo return two items both with refseq identifiers
refseqEntries =  mygeneinfo["refseq"]
addIdentifier(repo, item, refseqEntries["rna"], 'P639', reference2entrezGene) # P639 = RefSeq RNA ID
#for refseqRnaEntry in refseqEntries["rna"]:
#    print "adding refseq rna id: "+ refseqRnaEntry
#    addStatement(repo, item, 'P639', refseqRnaEntry, reference2entrezGene) #  P639 = RefSeq RNA ID

#Add chromosome
chromosome = mygeneinfo["genomic_pos"]["chr"]
chromosomes = dict()
chromosomes['1'] = "Q430258"
chromosomes['2'] = "Q638893"
chromosomes['3'] = "Q668633"
chromosomes['4'] = "Q836605"
chromosomes['5'] = "Q840741"
chromosomes['6'] = "Q540857"
chromosomes['7'] = "Q657319"
chromosomes['8'] = "Q572848"
chromosomes['9'] = "Q840604"
chromosomes['10'] = "Q840737"
chromosomes['11'] = "Q847096"
chromosomes['12'] = "Q847102"
chromosomes['13'] = "Q840734"
chromosomes['14'] = "Q138955"
chromosomes['15'] = "Q765245"
chromosomes['16'] = "Q742870"
chromosomes['17'] = "Q220677"
chromosomes['18'] = "Q780468"
chromosomes['19'] = "Q510786"
chromosomes['20'] = "Q666752"
chromosomes['21'] = "Q753218"
chromosomes['22'] = "Q753805"
chromosomes['22'] = "Q753805"
chromosomes['X'] = "Q61333"
chromosomes['Y'] = "Q202771"


print "Adding Chromosome: "+str(chromosome)

chromosomepage = pywikibot.ItemPage(repo, chromosomes[str(chromosome)])
addStatement(repo, item, 'P1057', chromosomepage, reference2entrezGene) # P1057 = chromosome FIXME: Link to Wikidata page
'''