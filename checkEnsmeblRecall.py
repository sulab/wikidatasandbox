# Before running this script
# run the following lines from the command line
# to obtain the data
# wget ftp://ftp.ensembl.org/pub/release-76/mysql/homo_sapiens_core_76_38/gene.txt.gz
# gzcat gene.txt.gz |grep ENSG|awk -F'\t' '{split($11,a,"["); split(a[1],b,"(");print $14 "\t" b[1]}' > ensemblGenes.tsv


import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
from pywikibot.data import api
import pprint

# filehandling
ensemblOutput = open("ensemblInWikiData.wiki", "w")
f = open('ensemblGenes.tsv', 'r')

# Connect to wikidata
site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

params = { 'action' :'wbsearchentities' ,
           'format' : 'json' ,
           'language' : 'en',
           'type' : 'item',
           'search': '',
         }
i = 0;
for line in f:
     outputLine = "";
     ensemblline = line.split('\t')
     ensemblId= ensemblline[0]
     ensemblName = ensemblline[1].rstrip()
     i+=1
     print str(i) + ": "+ ensemblId,
     outputLine = outputLine + ensemblId+"\t"  
     params['search']= ensemblName
     request = api.Request(site=site,**params)
     data = request.submit()
     pp = pprint.PrettyPrinter(indent=4)
     # pp.pprint(data['search'])
     for page in data['search']:
      #  print len(page),
      #  pp.pprint(page)
        print '[['+page["id"]+']]',
        outputLine = outputLine + '[['+page["id"]+']]' + "\t"
     print 
     ensemblOutput.write(outputLine+"\n")

f.close
ensemblOutput.close
