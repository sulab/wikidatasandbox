import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
import pprint
site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()
item = pywikibot.ItemPage(repo, "Q17388674")
dictionary = item.get()

claim = pywikibot.Claim(repo, u'P698')

# Get pubmed details from MygeneInfo
req=urllib2.Request("http://mygene.info/v2/gene/ENSG00000123374", None, {'user-agent':'Micelio'})
opener = urllib2.build_opener()
f = opener.open(req)
mygeneinfo = simplejson.load(f)
pathwayEntries =  mygeneinfo["pathway"]
claim = pywikibot.Claim(repo, u'P665') #KEGG ID
for pathwayEntry in pathwayEntries["kegg"]:
    print "adding Kegg id: "+pathwayEntry["id"]
    claim.setTarget(pathwayEntry["id"])
    item.addClaim(claim)
