import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
import pprint
site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()
item = pywikibot.ItemPage(repo, "Q17388674")
dictionary = item.get()

claim = pywikibot.Claim(repo, u'P698')

# Get pubmed details from MygeneInfo
req=urllib2.Request("http://mygene.info/v2/gene/ENSG00000123374", None, {'user-agent':'Micelio'})
opener = urllib2.build_opener()
f = opener.open(req)
mygeneinfo = simplejson.load(f)
pubmedEntries =  mygeneinfo["generif"]
claim = pywikibot.Claim(repo, u'P698') #Pubmed
for pubmedEntry in pubmedEntries:
    claim.setTarget(pubmedEntry["pubmed"])
    item.addClaim(claim)
