# Import the required libraries
import urllib2
# import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
from pywikibot.data import api
import pprint

# Login to wikidata
site = pywikibot.Site("wikidata", "wikidata")
# site = pywikibot.Site("wikidata", "test")
repo = site.data_repository()

def getItems(itemtitle):
  params = { 'action' :'wbsearchentities' , 'format' : 'json' , 'language' : 'en', 'type' : 'item', 'search': itemtitle}
  request = api.Request(site=site,**params)
  return request.submit()
	
def countItems(itemtitle):
   data = getItems(itemtitle)
   return len(data['search'])
   
def itemExists(itemtitle):
    itemFound = False
    if countItems(itemtitle) > 0:
        itemFound = True
    return itemFound
    
pp = pprint.PrettyPrinter(indent=4)

title = 'long intergenic non-protein coding RNA 850'

print itemExists(title)
print countItems(title)