import urllib2
import simplejson
import json
# -*- coding: utf-8  -*-
import pywikibot
from pywikibot.data import api
import pprint
site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()
item = pywikibot.ItemPage(repo, "Q17388674")
dictionary = item.get()
params = { 'action' :'wbsearchentities' ,
           'format' : 'json' ,              
           'language' : 'en',
           'limit' : '5',      #retreive five items
           'type' : 'item',
           'search': '',
         }
params['search']= "glyceraldehyde-3-phosphate dehydrogenase"
request = api.Request(site=site,**params)
data = request.submit()
pp = pprint.PrettyPrinter(indent=4)

if data['success']:
   pp.pprint(data['search']) 
